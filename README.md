# mono

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`mono` is a mono repo for developing the cobox stack. It contains all the project's modules, including core in `src/` and forks or adapted libraries in `lib/`.

## Setup

```
git clone --recursive git@gitlab.com:coboxcoop/mono.git && cd mono

yarn

./cobox app start --dev

./cobox seeder start --dev
```

## Usage
Check out our [development](./DEVELOPMENT.md) document for how to use the repository effectively. It makes use of git submodules and yarn workspaces for managing multiple packages used by two separate applications (server & seeder). If you want some help, please contact us at [info@cobox.cloud](mailto:info@cobox.cloud) and we'll be happy to give you a guided tour of the code base.

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
