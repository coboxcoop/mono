# Development

## Architecture Approach

CoBox follows a common P2P pattern of modularity (e.g. DAT projects, secure scuttlebutt). CoBox as a software is made up of [many modules](https://develop.cobox.cloud/modules.html).

To attempt to ease contribution without requiring lots of prior context, we decided to compose a mono-repo (parent/umbrella repo) which brings together all the modules. Modules we have written are in `/src` and modules we depend on are in `/lib`. This attempts to make feature development, collaboration and consistent review simpler for all involved. For this We use yarn workspaces and git submodules. Feature development of submodules is coordinated through mono-repo, not through the submodules standalone repo. Doing so allows feature development pinning mono to feature branches in various dependent submodules. It creates a strict development flow and is an opinionated approach and experiment in managing a highly modular JavaScript codebase.

This documentation will run you through the process we follow.


##### Update
- 09/06/21 - while git modules has significantly eased the process of reviewing pull requests and requires less write up when changing code in multiple locations, ultimately I have found it makes contributions by developers interested in the project more challenging as it requires a higher competency with git knowledge and use. For the time being we will leave the setup as it is, but will begin an internal discussion to make this more accessible in the future. Anyone trying to use the current setup finding it difficult, please [open an issue](gitlab.com/coboxcoop/mono/issues).

## Setup

### Starting the App

To get setup, simply do the following:

```sh
# clone the repo with all the submodules, then change into that directory (note using ssh, you can also close with http)
git clone --recursive git@gitlab.com:coboxcoop/mono.git && cd mono
# bundle all depedencies and link all submodules
yarn
# run the app
./cobox app start --dev
# if you want, you can also run the seeder
./cobox seeder start --dev
```

### Yarn Workspaces

We use yarn workspaces to link together packages in a single mono repo. This enables us to make changes to each individual package's raw source code without having to dive into `node_modules`. This is particularly useful when debugging issues that are occurring further down the stack, where we may need to make changes to the code in low level library. It means the changes we make are captured and made visible within our git repository, and prevents them from being over-written if we delete our `node_modules` folder and re-run `yarn`. The alternative would be to manually clone individual packages and link them together using `yarn link`. Yarn workspaces does this for us, so we don't need to do the heavy lifting. It works particularly well because we manage lots of smaller modules / packages. This works in tandem with our git workflow (see below).

### Git

#### Using Submodules

We use git submodules to manage code changes in multiple modules. This adds a few additional steps to your git workflow, but the benefits mean that as anyone coming to the codebase, you can get up-to-date with exactly where someone else left you without a discussion or needing additional context (e.g. written up in a MR), with a single command.

After pulling a branch in mono - such as `master` / `main`, or someone else's `feature/*` branch - the first command you should always run is: `git submodule update --init`. This will ensure all the submodules checkout to the correct commit for the mono-repo's parent branch. 

This will only work when the person responsible for maintaining that branch follows the git submodule workflow, which goes a little like this. When committing a set of changes to push to a remote, either for a single or group of submodules, you should always update the commit reference(s) in mono by adding a additional commit at the root of the project. This should also be pushed to mono's remote. As a result, when another developer wants to checkout to the same point as you, they simply pull the relevant branch in mono and run `git submodule update --init`.

Here's an example:

```sh
# checkout to a new feature branch in mono git repo
git checkout -b feature/git-workflow
# change to the submodule we want to make code changes to
cd src/server
# checkout to a feature branch in the git submodule server
git checkout -b feature/git-workflow
# make our changes to the code in src/server
# ...
# then we commit that code to the server git repo
git add .
git commit -m 'my new changes to the server submodule'
# and we push to the remote
git push origin feature/git-workflow
# now we change down to the project root of mono
cd ../../
# we will see a reference to src/server saying new commits
# we add that commit reference and commit it to the mono-repo
git add .
git commit -m 'update mono-repo commit reference for server submodule'
# finally we also push this to the mono repo remote
git push origin feature/git-workflow
# and you're done!
```

Now suppose I want to review your code, and you've made some changes to several submodules. Normally you'd need to tell me which modules you'd made changes to, and which branches I'd need to check out to manually. However, if the above workflow is followed, then I will be able checkout to precisely where I need to be to make a review your code, without us having a discussion. Here's how:

```sh
# inside the mono repo, I checkout to the branch that you've been working on
git checkout feature/git-workflow
# now i tell git to point to all the relevant commits in the submodules
git submodule update --init
# and im done! i can review your code as you have left it for me,
# and I can see easily all the changes you've made to multiple submodules 
# simply by looking at the submodule commit references in git log.
```

This makes working on multiple modules which require small changes much much easier. It also works really well in tandem with yarn workspaces, which means we can make changes to multiple dependencies both internal to our source code in `src`, and to the forked libraries we manage in `lib`.

### Starting Multiple Apps

To run two versions of the app, do the following:

```sh
# start one app with a specific storage location and under a certain port
./cobox app start --dev --config .coboxrc.dev.1
# start another app with a different storage location and a different port
./cobox app start --dev --config .coboxrc.dev.2
```

We use rc files, which can be passed as an argument to the CLI, to allow running multiple versions of the app at once alongside each-other to assist testing data replication across different peers. The same applies for the seeder app, however instead we use `.coboxseederrc.dev` files instead.

### Contribution

#### Merge Requests

It can be helpful to create a clear checklist of tasks, additions or smaller changes you've made which compose the feature, such as:

```
* [x] removed old unnecessary comments
* [ ] added controller for invites with create and index actions
* [x] unit tested invites controller
```

This helps the reviewer with an overview of all the component parts which compose the entire feature, and helps with documenting your own workflow.

#### Reviews

There are several sorts of review that can be done. You need to decide which are relevant and name them in communication.

**Please keep in mind** that while you're reviewing another persons work, you're in a position of quite some power over them.
I highly recommend:

- celebrating sweet pieces of code (if you learn a new trick, or love how elegant it is)
- naming what's working as well as what isn't
- talking about things which feel good about the change
- acknowledging the work that's been done if it's big

All of these things help make a review something you're doing _together_, and reminds you that there are people involved, and that criticism of the code that's been written is not a criticism of a person. Good vibes help us all go a lot further together.

##### Smoke Test

_If you turn the machine on an push a button, does smoke come out of it?_

In this sort of test, a peer:
- read the description of the piece of work in the PR
- spins up your branch
- click around to see if you can break it
  - try doing silly things (like 100 characters in a name)
  - poke parts of the app adjacent to this feature
  - see if it works if you start from scracth / start with an existing record
  - do the tests pass?
- if you find anything unexpected
  - post detailed notes on the PR
  - take screenshots

It can be helpful to create a clear checklist of things you tried like 

```
- [ ] added a parent to the top of the graph
- [x] added a parent to a person lower in the graph
- [x] added a child
```

##### Code Review

The purpose of this sort of review is to ensure the code is safe to merge.
This is measured on several axes:
- **danger** :fire:
  - is this code going to hurt anyone or their data if it's run ?
  - is it going to be incredibly resource intensive
- **technical debt** :wrench:
  - if someone needs to fix / modify this in the future, is it going to be easy to understand?
  - if something _complex_ (e.g. there are many paths through it) was added, is there a test around it?
  - if a beginner copies a pattern they've seen here, is the app going to become a mess?
  - is this adding repetition to the point that it will make maintenance hard (note: copying code 2-3 times can be appropriate)
  - if there's something messy, is it clearly commented as such, with all the info needed to improve the code in the future?
- **style** :sunglasses:
  - is the style consistent with the rest of the app (this sounds strange, but coherent style mean predicatable patterns which ease comprehension and reduce errors)
  - as an application grows, our style will evolve (same if new people join). Let's keep talking about it so that we're writing code that serves us well (i.t.o. comprehension)

_If you don't know what technical debt is yet, it's imperative we talk about it!_

##### Steps for a code review:
- read the description of the piece of work in the PR
- have a high-level look at the files changed
  - any surprises?
  - which files are core to this PR, versus periforal?
- read through the files, this can look like:
  - look for silly mistakes (leftover comments, TODOs, console.logs)
  - follow the flow of logic
  - this can be expensive i.t.o. time, but it can lead to a deeper understanding of the app, which pays off later
  - you don't need to understand everything 100%
  - look for confusing code
  - see if reading it a little longer helps
  - is that function doing too much? 
  - is this the right place for this functionality
  - are there unhelpful variable names like `_v`, `whakapapa`
- leave inline comments
  - clearly mark what sort of comment it is (:fire: = danger!, "style" = let's talk together, this is non-vital. Everything else is just a suggestion)
  - remember to celebrate successes, and leave cute emoji / [kaomoji](http://japaneseemoticons.me/excited-emoticons/)
  - if there's nothing to critique, skip to _merge_
- hand the review to the author
  - direct-message / @-mention the author with a link to the PR
- do follow up review
  - you're on reviewing this branch until it's merged
- merge
  - make sure you check someone has smoke-tested this before you merge. If no-one has, do this yourself now
  - select "delete branch"
  - decide whether to select "squash commits"

##### Making changes as the reviewer

It's best to leave changes in the hands of the author, but sometimes this isn't practical
e.g. it's a linting fixup, it's moving a file without changing the functionality, the person is on holiday

If you really do need to make a change consider:
- do you _really_ need to?
- messing with a persons branch can be really annoying e.g. they might be offline working on it
- is this thing blocking something else? if not, leave it
- making a branch off of this one, and opening a PR to the original branch
- do this _even if you plan to merge it yourself_
- this gives you a place to point to and talk about your changes
- this helps to be able to "show" a person the change you meant without having to do it everywhere. this sort of branch might even be thrown out later
- if it turns into a big change, you may need to take off your reviewer hat, and get someone else to review the final work

## Acknowledgments
This document has been heavily inspired by our friends at [Ahau](https://gitlab.com/ahau/process/-/raw/master/feature_development.md). 
