# Your CoBox Seeder

Congratulations on getting your very own CoBox Seeder Device!

CoBox Seeder comes setup with a 1TB hard drive mounted to the `cobox` user's `/home` directory (`/home/cobox`). This acts as your default storage location for all cobox data, and anything else you want to do using this machine.

To start your seeder, simply run the command:

```
cobox-seeder start
```

And follow the prompts, give your seeder a name (this is how others on the network will view this device, so it's useful it its clearly identifiable and unique).

 Once you've done this, you should be able to ping the server to check its online...

```
cobox-seeder whoami
```

And thats it! Your seeder is up and running. To learn more about how to use it, visit the documentation at https://docs.cobox.cloud.

We recommend you setup the seeder to autostart, just in case the machine ever is unplugged/turned off & rebooted. This means you won't have to manually login to the device to start the cobox-seeder software again. To do so, run the following script:

```
fail () {
  msg=$1
  echo "============"
  echo "Error: $msg" 1>&2
  exit 1
}

configure () {
  [ ! command -v 'pm2' &> /dev/null ] && npm install -g pm2 || fail "failed to install pm2. You might have to do this manually. Check out the pm2 and systemd documentation."

  pm2 startup

  sudo env PATH=$PATH:/home/cobox/.nvm/versions/node/v12.16.3/bin /home/cobox/.nvm/versions/node/v12.16.3/lib/node_modules/pm2/bin/pm2 startup systemd -u cobox --hp /home/cobox

  pm2 start "cobox-seeder start"

  pm2 save

  echo "Listing autostart processes by PM2..."
  pm2 ls
}

finish () {
  echo "============"
  echo "Startup configuration complete"
  echo "============"
  exit 1
}

configure
finish
```

To learn how to use `cobox-seeder`, you can find the documentation at https://docs.cobox.clou
If you want to get involved with the project, contact us via info@cobox.cloud.
If you're interested in making improvements and/or contributions to the code, visit the source code at https://gitlab.com/coboxcoop/mono/

Thanks for using CoBox! 
