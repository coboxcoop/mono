#!/bin/bash

# super quick late-night script to speed up deployment, leaves much to be desired

fail () {
  msg=$1
  echo "============"
  echo "Error: $msg" 1>&2
  exit 1
}

while getopts u:i:v: option
do
  case "${option}"
    in
    i) IP_ADDRESS=${OPTARG};;
    u) SSH_LOGIN=${OPTARG};;
    v) VERSION=${OPTARG};;
  esac
done

scp -rp ./dist/*.tar.gz $SSH_LOGIN@$IP_ADDRESS:/var/www/cobox.cloud/releases

scp -rp ./src/seeder/download.sh $SSH_LOGIN@$IP_ADDRESS:/var/www/cobox.cloud/releases/cobox-seeder-v$VERSION/download.sh

scp -rp ./src/server/download.sh $SSH_LOGIN@$IP_ADDRESS:/var/www/cobox.cloud/releases/cobox-v$VERSION/download.sh
