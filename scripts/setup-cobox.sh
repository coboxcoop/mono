#!/bin/bash

# WARNING:
# this will destroy any existing partitions on device /dev/sda
# and build a new partition table with a ext4 file system
# DO NOT USE if you want to do other than the default settings

# WARNING:
# this is a WIP, it is not yet ready to use as a proper script, its currently just a guide of commands

# NOTE:
# this script is for the first time setup of a cobox seeder device only
# DO NOT USE to run the cobox-seeder software

USER=cobox

DEVICE=/dev/sda
PARTITION=/dev/sda1

TMP_MNT=/mnt/$USER
DEFAULT_MNT=/home/$USER

fail () {
  msg=$1
  echo "============"
  echo "Error: $msg" 1>&2
  exit 1
}

setup () {
  [ ! "$BASH_VERSION" ] && fail "Please use bash instead"

  # ensure we can create the mount directory
  mkdir -p $DEFAULT_MNT || fail "Could not create directory $DEFAULT_MNT, try manually creating before running this script again."

  # update the system
  apt update || fail "unable to update properly, manually run 'apt update'"
  apt upgrade -y || fail "unable to upgrade, manually run 'apt upgrade'"
  apt install -y libtool || fail "unable to install libtool"

  fdisk $DEVICE
  # you'll have to manually create a partition, do the following:
  # p - print the partition table and check
  # d - delete until all existing partitions are removed (unless you want to do otherwise)
  # n - create a new partition, select 'primary', choose the partition size (default uses full available disk space)
  #   - if partition contains exfat signature, remove it
  # p - print to be sure, check start and end for partition size
  # w - write all changes (NOTE: this will delete all pre-existing partitions and reformat the drive)

  # build ext4 file system
  mkfs.ext4 $PARTITION
  # backup existing fstab
  cp -p /etc/fstab /etc/fstab.backup
  # get the UUID of the drive
  DRIVE_UUID="$(lsblk -po NAME,UUID | egrep -i $PARTITION | egrep -i -o [0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})"
  # TODO: fail if UUID is empty

  # append to /etc/fstab - this will automatically mount the drive on reboot
  echo "UUID=$DRIVE_UUID $DEFAULT_MNT ext4 defaults 0 1" | sudo tee -a /etc/fstab
  # output the diff
  diff /etc/fstab /etc/fstab.backup
  # TODO: if the diff isn't as expected, mv fstab.backup to fstab and fail with error message saying to do it manually

  # change to user cobox
  su - $USER

  # manually mount the drive
  mount $PARTITION $TMP_MNT || fail "Unable to mount drive, is it formatted correctly, and does it have a valid file system?"

  cd ..
  # copy entire user directory to from user directory to hard drive at tmp mountpoint
  cp -R $DEFAULT_MNT $TMP_MNT

  # ensure correct ownership of all existing files in /mnt/cobox
  find . \( -path ./lost+found \) -prune -o -user $USER -print0 | xargs -0 chown $USER

  # clear all user files in default mount
  rm -rf $DEFAULT_MNT

  # recreate empty mount point
  mkdir -p $DEFAULT_MNT

  # unmount from tmp mount point and remount hard drive as user home folder
  umount /dev/sda1
  mount /dev/sda1 $DEFAULT_MNT
}

install () {
  # install node version manager
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash

  # reload bashrc to use nvm cli
  source .bashrc

  # check if nvm install worked and bashrc was correctly reloaded
  [ !command -v 'nvm' &> /dev/null ] && fail "nvm could not be found, please restart your terminal session and check again. If that fails, install nodejs v12.16.3 manually."

  # install nodejs
  nvm install 12.16.3

  # install the seeder app
  npm install -g @coboxcoop/seeder
}

configure () {
  npm install -g pm2

  pm2 startup

  sudo env PATH=$PATH:/home/cobox/.nvm/versions/node/v12.16.3/bin /home/cobox/.nvm/versions/node/v12.16.3/lib/node_modules/pm2/bin/pm2 startup systemd -u cobox --hp /home/cobox

  pm2 start "cobox-seeder start"

  pm2 save
}

finish () {
  echo "============"
  echo "Setup complete"
  echo "============"
  echo "Start cobox-seeder using the following command 'cobox-seeder start'"
  exit 1
}

setup
install
configure
finish
