#!/usr/bin/env sh

# Builds binaries of a given application using warp-packer (https://github.com/dgiagio/warp)
# Run from the root of the project, specifying the application directory using -d
# Accepts a node version and warp version as arguments, defaults to latest stable release (as of 12.05.2020)
# usage: ./scripts/package.sh -d cobox-server -nv v12.16.3 -wv v0.3.0

fail () {
  msg=$1
  echo "============"
  echo "Error: $msg" 1>&2
  exit 1
}

cleanup () {
  echo "\nCleaning up... $1"
  rm -rf $1
}

while getopts a:d:w:n:v: option
do
  case "${option}"
    in
    a) APP_NAME=${OPTARG};;
    d) APP_DIR=${OPTARG};;
    n) NODE_VERSION=${OPTARG};;
    w) WARP_VERSION=${OPTARG};;
    v) APP_VERSION=${OPTARG};;
  esac
done

[ -z "$APP_DIR" ] && fail "Package to compile not specified, pass '-d' followed by the application directory"
[ ! -d ./$APP_DIR ] && fail "./$APP_DIR: no such file or directory"

ARCH=$(node -pe "require('os').arch()")
PLATFORM=$(node -pe "require('os').platform()")
DIST="$PLATFORM-$ARCH"

echo "Platform: $PLATFORM"
echo "Arch: $ARCH"
echo "Packaging: $APP_DIR-$VERSION\n"

[ -z "$NODE_VERSION" ] && NODE_VERSION="v12.16.3" && echo "node default set: $NODE_VERSION" || echo "node version: $NODE_VERSION"
[ -z "$WARP_VERSION" ] && WARP_VERSION="v0.3.0" && echo "warp default set: $WARP_VERSION" || echo "warp version: $WARP_VERSION"

[ -d ./builds/ ] && rm -r ./builds/

# ensure storage and mount directories are deleted up
rm -rf $APP_DIR/storage
rm -rf $APP_DIR/mnt

if [ $APP_DIR = 'src/server' ]; then
  echo "\nBuilding the UI for production"
  [ ! -x "$(command -v node)" ] && fail "node not installed"
  [ "$(node --version)" != $NODE_VERSION ] && fail "wrong version, please install specified version locally to build the UI"
  cd ./src/ui
  npm run build --silent --only=production
  cd ../..
fi

bundle () {
  RELEASE=$1
  echo "\nCopying LICENSE and README and assets/"
  cp ./$APP_DIR/LICENSE ./$APP_DIR/README.md ./dist/$RELEASE
  cp -r assets/ ./dist/$RELEASE

  echo "\nBuilding tarball for release"
  cd ./dist/$RELEASE
  tar cvzf ../$RELEASE.tar.gz .
  cd ../..
}

build () {
  BUILD_DIST=$1
  RELEASE=$APP_NAME-$APP_VERSION-$BUILD_DIST
  NODE_DIST=node-$NODE_VERSION-$BUILD_DIST
  EXEC='launch.sh'

  mkdir -p dist/$RELEASE
  mkdir -p builds/$RELEASE

  UI_PATH="$(pwd)/src/ui/dist/"

  cd builds/$RELEASE

  echo "\nDownloading $NODE_DIST and extracting to $(pwd)"
  wget -qO- https://nodejs.org/dist/$NODE_VERSION/$NODE_DIST.tar.xz | tar xJf -

  echo "\nCopying $APP_DIR to $(pwd)"
  rsync -avzrq --exclude 'node_modules' ../../$APP_DIR/* .

  echo "\nInstalling application dependencies\n"
  ./$NODE_DIST/bin/npm install --silent --only=production
  rm -r ./$NODE_DIST/include ./$NODE_DIST/share ./$NODE_DIST/lib
  rm ./$NODE_DIST/bin/npm ./$NODE_DIST/bin/npx

  if [ $APP_DIR = 'src/server' ]; then
    echo "\nCopying $UI_PATH to $(pwd)/public/dist"
    cp -r $UI_PATH ./public/dist
  fi

  cat > $EXEC <<-EOF
#!/bin/sh

APP_MAIN_JS=bin.js

DIR="\$(cd "\$(dirname "\$0")" ; pwd -P)"
NODE_EXE="\$DIR/$NODE_DIST/bin/node"
APP_MAIN_JS_PATH="\$DIR/\$APP_MAIN_JS"

exec "\$NODE_EXE" "\$APP_MAIN_JS_PATH" \$@
EOF

  chmod +x ./launch.sh || fail "chmod +x failed for ./launch.sh"

  cd ..

  echo "\nDownloading warp $WARP_VERSION and setting permissions\n"
  WARP_DIST="$([ $BUILD_DIST = "darwin-x64" ] && echo "macos-x64" || echo $BUILD_DIST )"
  curl -Lo ./warp-packer https://github.com/dgiagio/warp/releases/download/$WARP_VERSION/$DIST.warp-packer
  chmod +x ./warp-packer || fail "chmod +x failed"

  echo "\nCompiling $APP_NAME...\n"
  ./warp-packer --arch $WARP_DIST --input_dir ./$RELEASE --exec $EXEC --output ../dist/$RELEASE/$APP_NAME

  cd ..

  [ ! -f ./dist/$RELEASE/$APP_NAME ] && fail "warp build failed"

  echo "\n$APP_NAME compiled for $DIST\n"
  chmod +x ./dist/$RELEASE/$APP_NAME || fail "chmod +x ./dist/$RELEASE/$APP_NAME failed"
  file ./dist/$RELEASE/$APP_NAME
  du -hs ./dist/$RELEASE/$APP_NAME

  echo "Build complete: $RELEASE"
  echo "Binary location: ./dist/$RELEASE/$APP_NAME"

  bundle $RELEASE
  cleanup ./builds/$RELEASE
}

build "linux-x64"
build "darwin-x64"
cleanup ./builds
