#!/usr/bin/env sh

# build to use for linux-arm64 devices only using pkg (https://github.com/zeit/pkg)
# includes native addons (.node) files until warp support for arm64 or pkg supports bundling them in binary

# uses https://github.com/robertsLando/pkg-binaries patch to support build for arm64 devices
# check out: https://github.com/robertsLando/pkg-binaries/blob/master/utils/package.sh

WORK_DIR="$(pwd)"

fail () {
  msg=$1
  echo "============"
  echo "Error: $msg" 1>&2
  exit 1
}

cleanup () {
  echo "\nCleaning up... $1"
  rm -rf $1
}

APP_NAME="cobox-hub"
NODE_VERSION="12.2.0"
PLATFORM="linux"
ARCH="arm64"
PKG_VERSION='v2.6'
TMP_DIR=/tmp/$APP_NAME

# required node native addons
UTP_ADDON="node.napi.node"
SODIUM_ADDON="node.abi79.node"
LIBSODIUM="libsodium.so.23"
LEVELDOWN_ADDON="node.napi.armv8.node"


[ ! -x "$(command -v pkg)" ] && fail "pkg not installed: run 'yarn add --global pkg'"
[ ! -d $APP_NAME ] && fail "./src/$APP_NAME: no such file or directory"
[ -d $TMP_DIR ] && rm -r $TMP_DIR
[ -d ./dist/$APP_NAME ] && rm -r ./dist/$APP_NAME

VERSION=$(node -pe "require('./src/$APP_NAME/package.json').version")

bundle () {
  RELEASE=$1
  echo "\nCopying LICENSE and README and assets/"
  cp ./src/$APP_NAME/LICENSE ./src/$APP_NAME/README.md ./dist/$RELEASE
  cp -r assets/ ./dist/$RELEASE

  echo "\nBuilding tarball for release"
  cd ./dist/$RELEASE
  tar cvzf ../$RELEASE.tar.gz .
  cd ../..
}

build () {
  DIST=$1
  RELEASE=$APP_NAME-$VERSION-$DIST

  mkdir -p dist/$RELEASE
  mkdir -p $TMP_DIR

  echo "\nCopying $APP_NAME to $TMP_DIR"
  rsync -avzrq --exclude 'node_modules' ./src/$APP_NAME/* $TMP_DIR

  cd $TMP_DIR

  PKG_BINARY=$HOME/.pkg-cache/$PKG_VERSION/fetched-v$NODE_VERSION-$PLATFORM-$ARCH
  if [ ! -f $PKG_BINARY ]; then
    echo "\nDownloading $PKG_BINARY"
    wget -q "https://github.com/robertsLando/pkg-binaries/raw/master/$ARCH/fetched-v$NODE_VERSION-$PLATFORM-$ARCH" -O $PKG_BINARY
  else
    echo "\n$PKG_BINARY already exists, continuing..."
  fi

  echo "\nInstalling hub dependencies in $TMP_DIR\n"
  npm install --silent

  echo "\nCompiling $APP_NAME...\n"
  pkg package.json -t node$NODE_VERSION-$ARCH-$PLATFORM --out-path $WORK_DIR/dist/$RELEASE

  echo "Copying node native addons to ./dist/$RELEASE"
  cp ./node_modules/utp-native/prebuilds/linux-arm/$UTP_ADDON dist/$RELEASE/$UTP_ADDON
  cp ./node_modules/sodium-native/prebuilds/linux-arm/$SODIUM_ADDON dist/$RELEASE/$SODIUM_ADDON
  cp ./node_modules/sodium-native/prebuilds/linux-arm/$LIBSODIUM dist/$RELEASE/$LIBSODIUM
  # mkdir -p dist/$RELEASE/prebuilds/linux-arm64
  cp ./node_modules/leveldown/prebuilds/linux-arm64/$LEVELDOWN_ADDON dist/$RELEASE/$LEVELDOWN_ADDON
  cd $WORK_DIR

  bundle $RELEASE
}

build "linux-arm64"
