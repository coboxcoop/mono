#!/bin/bash

# Script for deleting all level-db LOCK files, deleting the mount directory and closing the port used by a particular instance of cobox. For use when the app has crashed, e.g. during a smoke-test, to allow it to start up again with ease.
# To use, type e.g.: ./scripts/crash-clean.sh -m mnt1 -s storage1 -p 9117


while getopts ":m:s:p:f." arg; do
   case $arg in
       m) mountDirectory=$OPTARG;;
       s) storageDirectory=$OPTARG;;
      p) port=$OPTARG;;
      f) file=$OPTARG;;
   esac
done

## TODO if no options are given, provide a hint
## TODO create a help command for using this script
## TODO make it possible to extract the mount point, storage directory & port number from the .coboxrc.dev file

# check if mountDirectory is a directory. If so, remove it
if [ $mountDirectory ] && [ -d $mountDirectory ] 
then
    echo removing mount directory at $mountDirectory
    rm -r ./$mountDirectory
fi

# check if storageDirectory is a directory. If so, remove all the LOCK files within it
if [ $storageDirectory ] && [ -d $storageDirectory ] 
then
    echo removing level-db LOCK files from $storageDirectory directory
    find ./$storageDirectory/1 -depth -name "LOCK" -exec rm {} \;
fi

# check if port is given. If so, kill the process running on it
if [ $port ] 
then
    echo closing port $port
    kill $(lsof -ti tcp:$port)
fi
