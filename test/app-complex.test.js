const App = require('@coboxcoop/server')
const getPort = require('get-port')
const { describe } = require('tape-plus')
const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const { tmp, cleanup } = require('./util')
const fs = require('fs')
const path = require('path')

describe('@coboxcoop/app', (context) => {
  context('two space to space', async (assert, next) => {
    const storage1 = tmp()
    const storage2 = tmp()
    const storage3 = tmp()
    const mount1 = tmp()
    const mount2 = tmp()
    const mount3 = tmp()
    const port1 = await getPort()
    const port2 = await getPort()
    const port3 = await getPort()

    const app1 = await App({ port: port1, storage: storage1, mount: mount1 })
    const app2 = await App({ port: port2, storage: storage2, mount: mount2 })
    const app3 = await App({ port: port3, storage: storage3, mount: mount3 })

    const client1 = new Client({ endpoint: buildBaseURL({ port: port1 }) })
    const client2 = new Client({ endpoint: buildBaseURL({ port: port2 }) })
    const client3 = new Client({ endpoint: buildBaseURL({ port: port3 }) })

    // generate one space
    let space1a = await client1.post('spaces', {}, { name: 'space1a' })
    await client1.post(['spaces', space1a.address, 'mounts'], {}, space1a)
    await write(path.join(mount1, space1a.name, '/hello.txt'), 'world')

    // generate a second space
    let space2a = await client2.post('spaces', {}, { name: 'space2a' })
    await client2.post(['spaces', space2a.address, 'mounts'], {}, space2a)
    await write(path.join(mount2, space2a.name, '/world.txt'), 'hello')

    // alice joins space1
    let alice = await client2.get('profile')
    let invite1 = await client1.post(['spaces', space1a.address, 'invites'], {}, alice)
    let space1b = await client2.post('spaces', {}, { code: invite1.content.code })
    await client2.post(['spaces', space1b.address, 'mounts'], {}, space1b)

    // bob joins space 2
    let bob = await client1.get('profile')
    let invite2 = await client2.post(['spaces', space2a.address, 'invites'], {}, bob)
    let space2b = await client1.post('spaces', {}, { code: invite2.content.code })
    await client1.post(['spaces', space2b.address, 'mounts'], {}, space2b)

    // swarm on both spaces
    await client1.post(['spaces', space1a.address, 'connections'], {}, space1a)
    await client1.post(['spaces', space2b.address, 'connections'], {}, space2b)

    await client2.post(['spaces', space1b.address, 'connections'], {}, space1b)
    await client2.post(['spaces', space2a.address, 'connections'], {}, space2a)

    await wait(2000)

    try {
      let data1 = await read(path.join(mount2, space1b.name, '/hello.txt'))
      assert.same(data1.toString(), 'world', 'replicates drive data')

      let data2 = await read(path.join(mount1, space2b.name, '/world.txt'))
      assert.same(data2.toString(), 'hello', 'replicates drive data')
    } catch (err) {
      assert.error(err, 'no error')
    }

    return done()

    function done () {
      app1.close((err, done) => {
        assert.error(err, 'no error on close')
        app2.close((err, done) => {
          assert.error(err, 'no error on close')
          cleanup([storage1, storage2], () => {
            process.exit(1)
          })
        })
      })
    }
  })
})

function write (filename, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filename, data, (err) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function read (filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function wait (ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms)
  })
}
