const App = require('@coboxcoop/server')
const Seeder = require('@coboxcoop/seeder')
const getPort = require('get-port')
const { describe } = require('tape-plus')
const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const { tmp, cleanup } = require('./util')
const fs = require('fs')
const path = require('path')

describe('@coboxcoop/seeder', (context) => {
  context('blind replication of spaces', async (assert, next) => {
    const storage1 = tmp()
    const storage2 = tmp()
    const storage3 = tmp()

    const port1 = await getPort()
    const port2 = await getPort()
    const port3 = await getPort()

    const mount1 = tmp()
    const mount2 = tmp()

    // start an app
    const app1 = await App({ port: port1, storage: storage1, mount: mount1 })
    const appClient1 = new Client({ endpoint: buildBaseURL({ port: port1 }) })

    // start a seeder
    const seeder = await Seeder({ port: port3, storage: storage3 })
    const seederClient = new Client({ endpoint: buildBaseURL({ port: port3 }) })

    // create a space, and write to its drive
    let space1a = await appClient1.post('spaces', {}, { name: 'space1' })
    await appClient1.post(['spaces', space1a.address, 'connections'], {}, space1a)
    await appClient1.post(['spaces', space1a.address, 'mounts'], {}, space1a)
    await write(path.join(mount1, space1a.name, '/hello.txt'), 'world')
    await wait(100)
    let connections = await appClient1.get(['spaces', space1a.address, 'connections'], {}, space1a)
    assert.same(connections.length, 1, 'app1 has no connections yet')
    assert.same(connections[0].address, space1a.address, "app1's only connection is app1")

    try {
      var data = await read(path.join(mount1, space1a.name, '/hello.txt'))
      assert.same(data.toString(), 'world', 'writes files correctly')
    } catch (err) {
      assert.error(err, 'no error')
    }

    // backup the space on our seeder and ensure we're connected and replicating
    await seederClient.post('replicators', {}, space1a)
    await wait(1000) // not sure why this works?
    await seederClient.post(['replicators', space1a.address, 'connections'], {}, space1a)

    // wait a second for seeder to replicate
    await wait(4000)
    connections = await appClient1.get(['spaces', space1a.address, 'connections'], {}, space1a)
    assert.same(connections.length, 2, 'app1 got a connection to seeder')
    if (connections.length < 2) process.exit(1)

    // first app unmounts and goes offline
    await appClient1.delete(['spaces', space1a.address, 'connections'], {}, space1a)
    await appClient1.delete(['spaces', space1a.address, 'mounts'], {}, space1a)

    await wait(2000)

    // start another app
    const app2 = await App({ port: port2, storage: storage2, mount: mount2 })
    const appClient2 = new Client({ endpoint: buildBaseURL({ port: port2 }) })

    // invite second app instance to join the space
    let alice = await appClient2.get('profile')
    let invite = await appClient1.post(['spaces', space1a.address, 'invites'], {}, alice)
    let space1b = await appClient2.post(['spaces'], {}, { code: invite.content.code })
    await appClient2.post(['spaces', space1b.address, 'connections'], {}, space1b)
    await appClient2.post(['spaces', space1b.address, 'mounts'], {}, space1b)

    // wait a second for seeder to replicate
    await wait(2000)

    try {
      let files = await ls(path.join(mount2, space1b.name))
      assert.same(files, ['hello.txt'], 'replicates file')
      let data = await read(path.join(mount2, space1b.name, '/hello.txt'))
      assert.same(data.toString(), 'world', 'replicates file content')
    } catch (err) {
      assert.error(err, 'no error')
    }

    await write(path.join(mount2, space1b.name, '/hello.txt'), 'mundo')
    await write(path.join(mount2, space1b.name, '/world.txt'), 'hello')
    let files = await ls(path.join(mount2, space1b.name))
    assert.same(files, ['hello.txt', 'world.txt'], 'writes files correctly')
    var data = await read(path.join(mount2, space1b.name, '/hello.txt'))
    assert.same(data.toString(), 'mundo', 'writes files correctly')
    var data = await read(path.join(mount2, space1b.name, '/world.txt'))
    assert.same(data.toString(), 'hello', 'writes files correctly')

    // wait a second for seeder to replicate
    await wait(2000)

    // second app goes offline, first app comes back online
    await appClient2.delete(['spaces', space1b.address, 'connections'], {}, space1b)
    await appClient2.delete(['spaces', space1b.address, 'mounts'], {}, space1b)

    await wait(2000)

    await appClient1.post(['spaces', space1a.address, 'connections'], {}, space1a)
    await appClient1.post(['spaces', space1a.address, 'mounts'], {}, space1a)

    // wait a second for seeder to replicate
    await wait(2000)

    try {
      let files = await ls(path.join(mount1, space1a.name))
      assert.same(files, ['hello.txt', 'world.txt'], 'replicates files')
      var data = await read(path.join(mount1, space1a.name, '/hello.txt'))
      assert.same(data.toString(), 'mundo', 'replicates file content')
      data = await read(path.join(mount1, space1a.name, '/world.txt'))
      assert.ok(data, 'data is defined')
      assert.same(data.toString(), 'hello', 'replicates file content')
    } catch (err) {
      assert.error(err, 'no error')
    }

    // unmount
    await appClient1.delete(['spaces', space1a.address, 'mounts'], {}, space1a)

    return done()

    function done () {
      app1.close((err, done) => {
        assert.error(err, 'no error on close')
        app2.close((err, done) => {
          assert.error(err, 'no error on close')
          // cleanup([storage1, storage2])
          // hack to exit the test, as long as we're using capture-exit,
          // we'll have to do this, so this test cannot be run as part of the suite
          process.exit(1)
        })
      })
    }
  })
})

function write (filename, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filename, data, (err) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function ls (dir) {
  return new Promise((resolve, reject) => {
    fs.readdir(dir, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function read (filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function wait (ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms)
  })
}

function close (app) {
  return new Promise((resolve, reject) => {
    app.close((err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}
