const App = require('@coboxcoop/server')
const getPort = require('get-port')
const { describe } = require('tape-plus')
const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const { tmp, cleanup } = require('./util')
const fs = require('fs')
const path = require('path')

describe('@coboxcoop/app', (context) => {
  context('one space to space', async (assert, next) => {
    const storage1 = tmp()
    const storage2 = tmp()
    const mount1 = tmp()
    const mount2 = tmp()
    const port1 = await getPort()
    const port2 = await getPort()

    const app1 = await App({ port: port1, storage: storage1, mount: mount1 })
    const app2 = await App({ port: port2, storage: storage2, mount: mount2 })

    const client1 = new Client({ endpoint: buildBaseURL({ port: port1 }) })
    const client2 = new Client({ endpoint: buildBaseURL({ port: port2 }) })

    let space1a = await client1.post('spaces', {}, { name: 'space1' })

    await client1.post(['spaces', space1a.address, 'connections'], {}, space1a)
    await client1.post(['spaces', space1a.address, 'mounts'], {}, space1a)

    await write(path.join(mount1, space1a.name, '/hello.txt'), 'world')

    let alice = await client2.get('profile')
    let invite = await client1.post(['spaces', space1a.address, 'invites'], {}, alice)

    let space1b = await client2.post(['spaces'], {}, { code: invite.content.code })
    await client2.post(['spaces', space1b.address, 'mounts'], {}, space1b)
    await client2.post(['spaces', space1b.address, 'connections'], {}, space1b)

    await wait(1000)

    try {
      let data = await read(path.join(mount2, space1b.name, '/hello.txt'))
      assert.same(data.toString(), 'world', 'replicates drive data')
    } catch (err) {
      assert.error(err, 'no error')
    }

    await write(path.join(mount2, space1b.name, '/hello.txt'), 'mundo')
    await wait(500)

    try {
      let data = await read(path.join(mount1, space1a.name, '/hello.txt'))
      assert.same(data.toString(), 'mundo', 'replicates drive data')
    } catch (err) {
      assert.error(err, 'no error')
    }

    return done()

    function done () {
      app1.close((err, done) => {
        assert.error(err, 'no error on close')
        app2.close((err, done) => {
          assert.error(err, 'no error on close')
          cleanup([storage1, storage2], next)
          // hack to exit the test, as long as we're using capture-exit,
          // we'll have to do this, so this test cannot be run as part of the suite
        })
      })
    }
  })
})

function write (filename, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filename, data, (err) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function read (filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

function wait (ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms)
  })
}
